$(function () {
   var userData = {};
   var userEmail = 'razzztaedd@gmail.com';
   var userUrlProfile  = 'http://146.185.154.90:8000/blog/'+ userEmail +'/profile';
   var userUrlPosts = 'http://146.185.154.90:8000/blog/'+ userEmail +'/posts';
   var userUrlSub = 'http://146.185.154.90:8000/blog/'+ userEmail +'/subscribe';
   var elementPopupUserData = $('#popup-user-data');
   var elementPopupFollow = $('#popup-follow');
    var updatePosts;

   var lastTimeMessage = '';

   // init
    getProfile();
    getPosts();

   function renderUserName() {
        $('#user-name').text(userData.firstName + ' ' + userData.lastName);
   }

   function renderPost(data) {
       var userName = $('<div class="message__user-name">').text(data.user.firstName + ' ' + data.user.lastName);
       var userMessage = $('<div class="message__inner-msg">').text(data.message);
       var messageWrap = $('<div class="message__item">').append(userName).append(userMessage);
       $('.message__list').prepend(messageWrap);
   }
   
   $('#get-user-data').on('submit', function (e) {
       e.preventDefault();
       userData.firstName = $(this).find('#get-user-data-firstName').val();
       userData.lastName = $(this).find('#get-user-data-lastName').val();
       editProfile();
       getPosts();
       elementPopupUserData.hide();
   });

    $('#follow-email').on('submit', function (e) {
        e.preventDefault();
        var value = $(this).find('#follow-email-data').val();
        subscribe({'email': value});
        getPosts();
        elementPopupFollow.hide();
    });

    $('#post-message').on('submit', function (e) {
       e.preventDefault();
       var value = $(this).find('#post-message-value').val();
        $(this).trigger('reset');
        addPost({"message":value});
    });

    $('.popup__close').on('click', function () {
        $(this).closest('.popup').hide();
    })

   $('#edit-user').on('click', function () {
      elementPopupUserData.show(300);
   });

   $('#add-follow').on('click', function () {
       elementPopupFollow.show(300);
   });

   function startTimeout(){
       clearTimeout(updatePosts);
       updatePosts = '';
       updatePosts = setTimeout(function () {
           getPostsDateTime(lastTimeMessage);
       }, 2000);
    }

    function getProfile() {
        $.ajax({
            type: 'GET',
            url: userUrlProfile
        })
            .then(function (value) {
                userData = value;
                renderUserName();
            })
    }

    function editProfile() {
        $.ajax({
            type: 'POST',
            url: userUrlProfile,
            data: userData
        }).then(function (value) {
            userData = value;
            renderUserName();
        })
    }

    function getPosts() {
        $.ajax({
            type: 'GET',
            url: userUrlPosts
        }).then(function (value) {
            for (var i = 0; i < value.length; i++){
                renderPost(value[i]);
                lastTimeMessage = value[i].datetime;
            }
            startTimeout();
        })
    }
    function getPostsDateTime(dateTime) {
        $.ajax({
            type: 'GET',
            url: userUrlPosts + '?datetime=' + dateTime
        }).then(function (value) {
            for (var i = 0; i < value.length; i++){
                renderPost(value[i]);
                lastTimeMessage = value[i].datetime;
            }
            startTimeout();
        })
    }

    function addPost(message) {
        clearInterval(updatePosts);
        $.ajax({
            type: "POST",
            url: userUrlPosts,
            data: message
        }).then(function (value) {
            getPostsDateTime(lastTimeMessage);
        })
    }
    
    function subscribe(email) {
        $.ajax({
            type: 'POST',
            url: userUrlSub,
            data: email
        }).then(function (value) {
            if (value.error){
                alert(value.error);
            }
        })
    }

    function unSubscribe() {
        $.ajax({
            type: 'POST',
            url: userUrlSub + '/delete',
        }).then(function (value) {
        })
    }
    
    function getSubscribe() {
        $.ajax({
            type: 'GET',
            url: userUrlSub
        }).then(function (value) {
        })
    }
});